use std::{io, thread, time::Duration};

use crossterm::{
    event::{self, DisableMouseCapture},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, LeaveAlternateScreen},
};
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Rect},
    style::{Color, Style, Modifier},
    text::Text,
    widgets::{Block, Borders, Paragraph},
    Terminal,
};

fn main() {
    //let ip_public = my_ip_public();
    let ip_public = "toto";
    let mut terminal = init_terminal().unwrap();

while true {
        terminal
        .draw(|f| {
            let size = f.size();
            let block = Block::default()
/*                 .title(
                    [
                        " Date : ",
                        &chrono::Local::now().format("%d/%m/%Y %H:%M:%S").to_string(),
                        " - IP public : ",
                        &ip_public,
                        " ",
                    ]
                    .join(""),
                ) */
                .borders(Borders::ALL)
                .title_alignment(Alignment::Left);

                let date  = Paragraph::new(chrono::Local::now().format("%d/%m/%Y %H:%M:%S").to_string()).alignment(Alignment::Center);
                let rect = Rect::new(2, (size.height / 2) - 1, size.width - 4,1 );

            f.render_widget(block, size);
            f.render_widget(date, rect);
            thread::sleep(Duration::from_secs(1));

        })
        .unwrap();
}



    ctrlc::set_handler(move || {
        restore_terminal(&mut terminal);
    })
    .unwrap();
}

/// Initlaisaton du terminal
fn init_terminal() -> Result<Terminal<CrosstermBackend<io::Stdout>>, io::Error> {
    println!("init");
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal: Terminal<CrosstermBackend<io::Stdout>> = Terminal::new(backend).unwrap();
    enable_raw_mode()?;
    terminal.clear().unwrap();
    Ok(terminal)
}

/// Restoration du terminal
fn restore_terminal(terminal: &mut Terminal<CrosstermBackend<io::Stdout>>) {
    // restore terminal
    disable_raw_mode().unwrap();
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )
    .unwrap();
    terminal.show_cursor().unwrap();
}

/// Récupérer son ip public
fn my_ip_public() -> String {
    let response = reqwest::blocking::get("https://api.ipify.org/");
    let ip_public = match response {
        Ok(response_result) => match response_result.status() {
            reqwest::StatusCode::OK => response_result.text_with_charset("utf-8").unwrap(),
            _ => String::from("inconue"),
        },
        Err(error) => error.to_string(),
    };

    ip_public
}
